package com.healios.testapp.core.di;

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.healios.testapp.data.db.AppDatabase
import com.healios.testapp.data.db.CommentDao
import com.healios.testapp.data.db.PostDao
import com.healios.testapp.data.db.UserDao
import com.healios.testapp.data.remote.CommentRemoteDataSource
import com.healios.testapp.data.remote.PostRemoteDataSource
import com.healios.testapp.data.remote.UserRemoteDataSource
import com.healios.testapp.data.repository.CommentRepository
import com.healios.testapp.data.repository.PostRepository
import com.healios.testapp.data.repository.UserRepository
import com.healios.testapp.data.retrofit.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson): Retrofit = Retrofit.Builder()
        .baseUrl("http://jsonplaceholder.typicode.com/")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService =
        retrofit.create(ApiService::class.java)

    @Singleton
    @Provides
    fun provideUserRemoteDataSource(apiService: ApiService) =
        UserRemoteDataSource(apiService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideUserDao(db: AppDatabase) = db.userDao()

    @Singleton
    @Provides
    fun provideCommentDao(db: AppDatabase) = db.commentDao()

    @Singleton
    @Provides
    fun providePostDao(db: AppDatabase) = db.postDao()

    @Singleton
    @Provides
    fun providePostRepository(
        remoteDataSource: PostRemoteDataSource,
        localDataSource: PostDao
    ) =
        PostRepository(remoteDataSource, localDataSource)

    @Singleton
    @Provides
    fun provideUserRepository(
        remoteDataSource: UserRemoteDataSource,
        localDataSource: UserDao
    ) =
        UserRepository(remoteDataSource, localDataSource)

    @Singleton
    @Provides
    fun provideCommentRepository(
        remoteDataSource: CommentRemoteDataSource,
        localDataSource: CommentDao
    ) =
        CommentRepository(remoteDataSource, localDataSource)
}