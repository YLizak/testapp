package com.healios.testapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "comments")
data class Comment(
    val body: String,
    val email: String,
    @PrimaryKey val id: Int,
    val name: String,
    val postId: Int
)