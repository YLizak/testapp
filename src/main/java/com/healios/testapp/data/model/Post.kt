package com.healios.testapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "posts")
data class Post(
    val body: String,
    @PrimaryKey val id: Int,
    val title: String,
    val userId: Int
)