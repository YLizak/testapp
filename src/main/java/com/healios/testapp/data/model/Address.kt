package com.healios.testapp.data.model

import androidx.room.Embedded

data class Address(
    val city: String,
    @Embedded(prefix = "geo") val geo: Geo,
    val street: String,
    val suite: String,
    val zipcode: String
)