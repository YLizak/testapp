package com.healios.testapp.data.model

import androidx.room.Entity

@Entity(tableName = "geos")
data class Geo(
    val lat: String,
    val lng: String
)