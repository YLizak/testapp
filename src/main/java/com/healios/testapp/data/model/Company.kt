package com.healios.testapp.data.model

import androidx.room.Entity

@Entity(tableName = "companies")
data class Company(
    val bs: String,
    val catchPhrase: String,
    val name: String
)