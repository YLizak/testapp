package com.healios.testapp.data.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class User(
    @Embedded(prefix = "address") val address: Address,
    @Embedded(prefix = "company") val company: Company,
    val email: String,
    @PrimaryKey val id: Int,
    val name: String,
    val phone: String,
    val username: String,
    val website: String
)