package com.healios.testapp.data.retrofit

import com.healios.testapp.data.model.Comment
import com.healios.testapp.data.model.Post
import com.healios.testapp.data.model.User
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("posts")
    suspend fun getAllPosts() : Response<List<Post>>

    @GET("users")
    suspend fun getAllUsers(): Response<List<User>>

    @GET("comments")
    suspend fun getAllComments(): Response<List<Comment>>

    @GET("posts/{id}")
    suspend fun getPost(@Path("id") id: Int): Response<Post>

    @GET("users/{id}")
    suspend fun getUser(@Path("id") id: Int): Response<User>

    @GET("comments/{id}")
    suspend fun getComment(@Path("id") id: Int): Response<Comment>
}