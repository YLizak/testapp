package com.healios.testapp.data.repository

import com.healios.testapp.core.utils.performGetOperation
import com.healios.testapp.data.db.CommentDao
import com.healios.testapp.data.remote.CommentRemoteDataSource
import javax.inject.Inject

class CommentRepository @Inject constructor(
    private val remoteDataSource: CommentRemoteDataSource,
    private val localDataSource: CommentDao
) {
    fun getComment(id: Int) = performGetOperation(
        databaseQuery = { localDataSource.getComment(id) },
        networkCall = { remoteDataSource.getComment(id) },
        saveCallResult = { localDataSource.insert(it) }
    )

    fun getComments() = performGetOperation(
        databaseQuery = { localDataSource.getAllComments() },
        networkCall = { remoteDataSource.getComments() },
        saveCallResult = { localDataSource.insertAll(it) }
    )
}
