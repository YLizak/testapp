package com.healios.testapp.data.repository

import com.healios.testapp.core.utils.performGetOperation
import com.healios.testapp.data.db.UserDao
import com.healios.testapp.data.remote.UserRemoteDataSource
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val remoteDataSource: UserRemoteDataSource,
    private val localDataSource: UserDao
) {
    fun getUser(id: Int) = performGetOperation(
        databaseQuery = { localDataSource.getUser(id) },
        networkCall = { remoteDataSource.getUser(id) },
        saveCallResult = { localDataSource.insert(it) }
    )

    fun getUsers() = performGetOperation(
        databaseQuery = { localDataSource.getAllUsers() },
        networkCall = { remoteDataSource.getUsers() },
        saveCallResult = { localDataSource.insertAll(it) }
    )
}
