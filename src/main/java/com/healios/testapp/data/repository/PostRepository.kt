package com.healios.testapp.data.repository

import com.healios.testapp.core.utils.performGetOperation
import com.healios.testapp.data.db.PostDao
import com.healios.testapp.data.remote.PostRemoteDataSource
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val remoteDataSource: PostRemoteDataSource,
    private val localDataSource: PostDao

) {
    fun getPost(id: Int) = performGetOperation(
        databaseQuery = { localDataSource.getPost(id) },
        networkCall = { remoteDataSource.getPost(id) },
        saveCallResult = { localDataSource.insert(it) }
    )

    fun getPosts() = performGetOperation(
        databaseQuery = { localDataSource.getAllPosts() },
        networkCall = { remoteDataSource.getPosts() },
        saveCallResult = { localDataSource.insertAll(it) }
    )
}
