package com.healios.testapp.data.remote

import com.healios.testapp.data.retrofit.ApiService
import javax.inject.Inject

class PostRemoteDataSource @Inject constructor(
    private val apiService: ApiService
): BaseDataSource() {

    suspend fun getPosts() = getResult { apiService.getAllPosts() }

    suspend fun getPost(id : Int) = getResult { apiService.getPost(id) }
}
