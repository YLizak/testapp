package com.healios.testapp.data.remote

import com.healios.testapp.data.retrofit.ApiService
import javax.inject.Inject

class CommentRemoteDataSource @Inject constructor(
    private val apiService: ApiService
): BaseDataSource() {

    suspend fun getComments() = getResult { apiService.getAllComments() }

    suspend fun getComment(id : Int) = getResult { apiService.getComment(id) }
}
