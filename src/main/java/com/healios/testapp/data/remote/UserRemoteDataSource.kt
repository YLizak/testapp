package com.healios.testapp.data.remote

import com.healios.testapp.data.retrofit.ApiService
import javax.inject.Inject

class UserRemoteDataSource @Inject constructor(
    private val apiService: ApiService
): BaseDataSource() {

    suspend fun getUsers() = getResult { apiService.getAllUsers() }

    suspend fun getUser(id : Int) = getResult { apiService.getUser(id) }
}
