package com.healios.testapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.healios.testapp.data.model.Comment

@Dao
interface CommentDao {

    @Query("SELECT * FROM comments")
    fun getAllComments() : LiveData<List<Comment>>

    @Query("SELECT * FROM comments WHERE id = :id")
    fun getComment(id: Int): LiveData<Comment>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(users: List<Comment>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: Comment)
}
