package com.healios.testapp.presentation.postdetail

import androidx.lifecycle.*
import com.healios.testapp.core.utils.Resource
import com.healios.testapp.data.repository.CommentRepository
import com.healios.testapp.data.repository.PostRepository
import com.healios.testapp.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PostDetailViewModel @Inject constructor(
    private val postRepository: PostRepository,
    private val userRepository: UserRepository,
    private val commentRepository: CommentRepository
) : ViewModel() {

    private val _id = MutableLiveData<Int>()

    private val _fullPost = _id.switchMap { id -> MediatorLiveData<Resource<FullPost>>()
        .apply {
            val postL = postRepository.getPost(id)
            val usersL = userRepository.getUsers()
            val commentsL = commentRepository.getComments()

            fun update() {
                val post = if (postL.value?.status == Resource.Status.SUCCESS) postL.value else return
                val users = if (usersL.value?.status == Resource.Status.SUCCESS) usersL.value else return
                val comments = if (commentsL.value?.status == Resource.Status.SUCCESS) commentsL.value else return

                value = Resource.success(
                    FullPost(post?.data!!,
                        users?.data?.filter { user -> user.id == post.data.userId }?.single(),
                        comments?.data?.filter { comment -> comment.postId == id }
                    ))
            }

            addSource(postL) { update() }
            addSource(usersL) { update() }
            addSource(commentsL) { update() }

            update()
        }
    }

    val fullPost: LiveData<Resource<FullPost>> = _fullPost


    fun start(id: Int) {
        _id.value = id
    }
}