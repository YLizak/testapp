package com.healios.testapp.presentation.postdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.healios.testapp.R
import com.healios.testapp.core.utils.Resource
import com.healios.testapp.databinding.PostDetailFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PostDetailFragment : Fragment() {

    private lateinit var binding: PostDetailFragmentBinding
    private val viewModel: PostDetailViewModel by viewModels()
    private lateinit var adapter: CommentsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.post_detail_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getInt("id")?.let { viewModel.start(it) }
        setupObservers()
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        adapter = CommentsAdapter()
        binding.commentsRecycler.layoutManager = LinearLayoutManager(requireContext())
        binding.commentsRecycler.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.fullPost.observe(viewLifecycleOwner,{
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    bindPost(it.data!!)
                    binding.progressBar.visibility = View.GONE
                    binding.postBlock.visibility = View.VISIBLE
                }

                Resource.Status.ERROR ->
                    Toast.makeText(activity, it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.postBlock.visibility = View.GONE
                }
            }
        })
    }

    private fun bindPost(post: FullPost) {
        binding.post = post
        post.comments?.let { adapter.setItems(ArrayList(it)) }
    }
}