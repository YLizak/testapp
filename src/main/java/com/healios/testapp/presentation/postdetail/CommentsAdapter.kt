package com.healios.testapp.presentation.postdetail

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.healios.testapp.data.model.Comment
import com.healios.testapp.databinding.ItemCommentBinding

class CommentsAdapter : RecyclerView.Adapter<CommentsAdapter.CommentViewHolder>() {

    private val items = ArrayList<Comment>()

    fun setItems(items: ArrayList<Comment>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val binding: ItemCommentBinding =
            ItemCommentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CommentViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) =
        holder.bind(items[position])

    inner class CommentViewHolder(
        private val itemBinding: ItemCommentBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        private lateinit var post: Comment

        @SuppressLint("SetTextI18n")
        fun bind(item: Comment) {
            this.post = item
            itemBinding.title.text = item.name
            itemBinding.body.text = item.body
            itemBinding.email.text = item.email
        }
    }
}
