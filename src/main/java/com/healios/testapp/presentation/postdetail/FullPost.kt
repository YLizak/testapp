package com.healios.testapp.presentation.postdetail

import com.healios.testapp.data.model.Comment
import com.healios.testapp.data.model.Post
import com.healios.testapp.data.model.User

data class FullPost(
    val post: Post,
    val author: User?,
    val comments: List<Comment>?
)
