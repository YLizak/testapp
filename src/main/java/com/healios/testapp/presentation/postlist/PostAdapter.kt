package com.healios.testapp.presentation.postlist

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.healios.testapp.data.model.Post
import com.healios.testapp.data.repository.UserRepository
import com.healios.testapp.databinding.ItemPostBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

class PostAdapter(private val listener: PostItemListener) : RecyclerView.Adapter<PostAdapter.PostViewHolder>() {

    interface PostItemListener {
        fun onClickedPost(postId: Int)
    }

    private val items = ArrayList<PostItem>()

    fun setItems(items: ArrayList<PostItem>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val binding: ItemPostBinding =
            ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PostViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) =
        holder.bind(items[position])

    inner class PostViewHolder(
        private val itemBinding: ItemPostBinding,
        private val listener: PostAdapter.PostItemListener
    ) : RecyclerView.ViewHolder(itemBinding.root),
        View.OnClickListener {

        private lateinit var post: PostItem

        init {
            itemBinding.root.setOnClickListener(this)
        }

        @SuppressLint("SetTextI18n")
        fun bind(item: PostItem) {
            this.post = item
            itemBinding.title.text = item.title
            itemBinding.userName.text = item.userName
        }

        override fun onClick(v: View?) {
            listener.onClickedPost(post.id)
        }
    }
}
