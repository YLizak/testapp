package com.healios.testapp.presentation.postlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.healios.testapp.R
import com.healios.testapp.core.utils.Resource
import com.healios.testapp.data.model.Post
import com.healios.testapp.data.model.User
import com.healios.testapp.databinding.PostListFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import java.util.ArrayList

@AndroidEntryPoint
class PostListFragment : Fragment(), PostAdapter.PostItemListener {

    private lateinit var binding: PostListFragmentBinding
    private val viewModel: PostListViewModel by viewModels()
    private lateinit var adapter: PostAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.post_list_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
    }

    private fun setupRecyclerView() {
        adapter = PostAdapter(this)
        binding.postsRecycler.layoutManager = LinearLayoutManager(requireContext())
        binding.postsRecycler.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.posts.observe(viewLifecycleOwner, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    viewModel.users.observe(viewLifecycleOwner, { it1 ->
                        when (it1.status) {
                            Resource.Status.SUCCESS -> {
                                binding.progressBar.visibility = View.GONE
                                if (!it.data.isNullOrEmpty() && !it1.data.isNullOrEmpty())
                                    adapter.setItems(createPostItemList(it.data, it1.data))
                            }
                            Resource.Status.ERROR ->
                                Toast.makeText(requireContext(), it1.message, Toast.LENGTH_SHORT).show()

                            Resource.Status.LOADING ->
                                binding.progressBar.visibility = View.VISIBLE
                        }
                    })
                }
                Resource.Status.ERROR ->
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING ->
                    binding.progressBar.visibility = View.VISIBLE
            }
        })
    }

    private fun createPostItemList(
        posts: List<Post>?,
        users: List<User>?
    ): ArrayList<PostItem> {
        val postItems = ArrayList<PostItem>()
        posts?.forEach {
            postItems.add(PostItem(it.id, it.title,
                users?.filter { user -> user.id == it.userId }
                ?.single()?.username ?: ""))
        }
        return postItems
    }

    override fun onClickedPost(postId: Int) {
        findNavController().navigate(
            R.id.action_postListFragment_to_postDetailFragment,
            bundleOf("id" to postId)
        )
    }
}