package com.healios.testapp.presentation.postlist

data class PostItem (
    val id: Int,
    val title: String,
    val userName: String
)