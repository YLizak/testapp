package com.healios.testapp.presentation.postlist

import androidx.lifecycle.ViewModel
import com.healios.testapp.data.repository.PostRepository
import com.healios.testapp.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PostListViewModel @Inject constructor(
    private val postRepository: PostRepository,
    private val userRepository: UserRepository
) : ViewModel() {

    val posts = postRepository.getPosts()

    val users = userRepository.getUsers()
}
